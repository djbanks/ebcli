FROM ubuntu:focal

ENV DEBIAN_FRONTEND=noninteractive

# Install packages
RUN apt-get update && apt-get install -y bc curl groff jq less unzip zip \
    git python3-pip

# Install the EB CLI
RUN pip install awsebcli